/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  useColorScheme,
  View,
} from 'react-native';
import AzureAuth from 'react-native-azure-auth';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import 'react-native-devsettings';

function App(): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const azureLogin = async () => {
    const azureAuth = new AzureAuth({
      clientId: 'afa610dc-d333-425c-99d2-317da1f0dba7',
    });
    try {
      const tokens = await azureAuth.webAuth.authorize({
        scope: 'openid profile email User.Read',
      });
      console.log(tokens);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Button title="Press me" onPress={azureLogin} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default App;
